using System;

namespace PhoneNumberValidator
{
    //Please do not change the signature of class methods
    public class PhoneNumberValidator
    {
        static void Main(string[] args)
        {
            // call the functions to validate phone number and display the status
            string mobileNo = GetInput();
            int result = ValidatePhoneNumber(mobileNo); 
            DisplayResult(result);
        }
        public static string GetInput()
        {
            // get phonenumber as input
            string phoneNo = "";
            phoneNo = Console.ReadLine();
            return phoneNo;
            
        }
        public static void DisplayResult(int result)
        {
            // display status of phone number valid or invalid
            if(result == 1)
            {
                Console.WriteLine("Valid");
            }
            else
            {
                Console.WriteLine("In Valid");
            }
        }
        public static int ValidatePhoneNumber(string input)
        {
            // validate phone-number
            // for null or empty value, function should return -1
            // for invalid phone-number, function should return 0
            // for valid phone-number, function should return 1          
            int dashCount = 0;
            int numCount = 0;         
            if(input == null || input == "")
            {
                return -1;
            }
            else 
            {
                int phoneNoLength = input.Length;
                for (int i = 0; i < phoneNoLength; i++)
                {
                    if (input[i] == '-')
                    {
                        dashCount++;
                    }
                    if (Char.IsDigit(input[i]))
                    {
                        numCount++;
                    }
                }
                if(numCount+dashCount == phoneNoLength && numCount == 10)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
                
            }
      
           
        }
    }
}
